# Copyright (C) 2021 Marco Cesati

def bitsize(N):
	v = max(ceil(log(N+1,2)),1)
	return v

def hexprint(S,N):
	print("%s%x [%d]" % (S,N,bitsize(N)))

def get_random_prime(e):
	return random_prime(lbound=2^(e-1), n=2^e)

def get_correl_primes(n, K, T):
	for i in xsrange(1000):
		q = get_random_prime(n)
		r = get_random_prime(n)
		for k in xsrange(2,K+1):
			p = r + (k * q - r) % T
			if is_prime(p):
				return [p,q,k]
	return [0,0,0]

def init(alpha, K, c):
	while True:
		T = get_random_prime(alpha-c)
		[p, q, k] = get_correl_primes(alpha, K, T)
		if p != 0:
			return [ T, p, q, k ]

def recover_ph2(N,T,NmT,k,start,end):
	a1 = (NmT/k).sqrt()
	b1 = (a1 * k)
	a2 = T - a1
	b2 = (a2 * k)
	a1 = a1.lift()
	a2 = a2.lift()
	b1 = b1.lift()
	b2 = b2.lift()
	c1 = (N-a1*b1)/T
	c2 = (N-a2*b2)/T
#	print("k=%d a1=0x%x b1=0x%x c1=0x%x a2=0x%x b2=0x%x c2=0x%x" % (k, a1, b1, c1, a2, b2, c2))
	for C in xsrange(start,end+1):
		D1=(b1-a1-C*T)^2-4*T*(c1-b1*C)
		if D1.is_square():
#			print("square Delta for k=%d C=%d a1=0x%x b1=0x%x c1=0x%x" % (k,C,a1,b1,c1))
			D1 = sqrt(D1)
			s1 = (C*T+a1-b1+D1)/(2*T)
			s2 = (C*T+a1-b1-D1)/(2*T)
			if s1.is_integral():
#				print("candidate pair: (%d,%d) k=%d C=%d" % (s1, C-s1, k, C))
				p = s1*T+b1
				q = (C-s1)*T+a1
				if N == p*q:
					return [p,q]
			if s2.is_integral():
#				print("candidate pair: (%d,%d) k=%d C=%d" % (s2, C-s2, k, C))
				p = s2*T+b1
				q = (C-s2)*T+a1
				if N == p*q:
					return [p,q]
		D2=(b2-a2-C*T)^2-4*T*(c2-b2*C)
		if D2.is_square():
#			print("square Delta for k=%d C=%d a2=0x%x b2=0x%x c2=0x%x" % (k,C,a2,b2,c2))
			D2 = sqrt(D2)
			s1 = (C*T+a2-b2+D2)/(2*T)
			s2 = (C*T+a2-b2-D2)/(2*T)
			if s1.is_integral():
#				print("candidate pair: (%d,%d) k=%d C=%d" % (s1, C-s1, k, C))
				p = s1*T+b2
				q = (C-s1)*T+a2
				if N == p*q:
					return [p,q]
			if s2.is_integral():
#				print("candidate pair: (%d,%d) k=%d C=%d" % (s2, C-s2, k, C))
				p = s2*T+b2
				q = (C-s2)*T+a2
				if N == p*q:
					return [p,q]
	return [0,0]


def recover(N, T):
	GFT = IntegerModRing(T,is_field=True)
	NmT = GFT(N)
	start = floor(sqrt(2*(floor(N/(T*T))-1)))
	end = ceil(N/(T*T))
	k=2
	while True:
		if GFT(NmT/k).is_square():
			[p, q] = recover_ph2(N,T,NmT,k,start,end)
			if p != 0:
				return [p, q]
		k += 1
	return [p, q]

