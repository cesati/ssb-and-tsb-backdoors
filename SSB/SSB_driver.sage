# Copyright (C) 2021 Marco Cesati

from sage.doctest.util import Timer
import csv
load("SSB.sage")

#[alpha, K, c] = [  64, 10,  3]  # 128-bit moduli
#[alpha, K, c] = [ 128, 30,  5]  # 256-bit moduli
#[alpha, K, c] = [ 512, 300,  7]  # 1024-bit moduli
#[alpha, K, c] = [1024,  1000, 7]  # 2048-bit moduli
#[alpha, K, c] = [2048,  3000, 7]  # 4096-bit modulu
#print("alpha=%d\nK=%d\nc=%d" % (alpha, K, c))

timer = Timer()
csvfile = open('ssb_timing.csv','w',newline='')
#csvfile = open('ssb_timing.csv','wb') # older SageMath
csvwriter = csv.writer(csvfile, delimiter=',')

rep = 10

def run(alpha,K,c,i):
	timer.start()
	[T, p, q, k] = init(alpha, K, c)
	N=p*q
	time_gener = timer.stop().cputime
	print("T=0x%x\np=0x%x\nq=0x%x\nk=%d" % (T,p,q,k))
	print("N=0x%x" % N)
	print("[TIMING] Generation: %.3f seconds" % time_gener)
	timer.start()
	[p1, q1] = recover(N, T)
	time_recover = timer.stop().cputime
	if p1*q1 == p*q:
		print("Found factors:\n  0x%x *\n  0x%x" % (p1, q1))
	print("[TIMING] Recover: %.3f seconds" % time_recover)
	csvwriter.writerow([alpha,c,i,K,k,time_gener,time_recover])
	csvfile.flush()

#alpha=512
c=7
for K in xsrange(100,2000,100):
	for x in xsrange(rep):
		run(alpha,K,c,x)

csvfile.close()
