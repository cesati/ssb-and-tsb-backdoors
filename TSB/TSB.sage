# Copyright (C) 2021 Marco Cesati

def bitsize(N):
	v = max(ceil(log(N+1,2)),1)
	return v

def hexprint(S,N):
	print("%s%x [%d]" % (S,N,bitsize(N)))

def get_random_prime(e):
	return random_prime(lbound=2^(e-1), n=2^e)

def get_correl_sq_primes(n, T, K):
	for i in xsrange(1000):
		q1 = get_random_prime(n);
		p = get_random_prime(n);
		for h in xsrange(2,K+1):
			hs = h * h
			q2 = p + (hs * q1 - p) % T
			if is_prime(q2):
				return [q1,q2,h]
	return [0,0,0]

def get_correl_prime(n, q, h, T, K, c):
	for i in xsrange(1,2*K+1):
		k = 2 + ZZ.random_element(K-2)
		mp1 = (k * h * q) % T
		end = 2^(2*c-2)
		start = 2^(c-3)
		mid = ZZ.random_element(start, end)
		p = (mid-1) * T + mp1
		for pi in xsrange(mid, end):
			p = p + T;
			if is_prime(p):
				return [ p, k ]
		p = (start-1)*T + mp1
		for pi in xsrange(start,mid):
			p = p + T;
			if is_prime(p):
				return [ p, k ]
	return [ 0, 0 ]

def init(alpha, K, c):
	T = get_random_prime(alpha-c)
	[q1, q2, h] = get_correl_sq_primes(alpha, T, K)
	not_found = True
	iter=0
	while not_found and (iter < alpha/10):
		[ p1, k1 ] = get_correl_prime(alpha, q2, h, T, K, c)
		not_found = (p1 == 0) or (gcd(k1,h) != 1)
		iter += 1
	if not_found:
		return [ 0, 0, 0, 0, 0, 0, 0, 0]
	not_found = True
	iter=0
	while not_found and (iter < alpha/10):
		[ p2, k2 ] = get_correl_prime(alpha, q1, 1, T, K, c)
		not_found = (p2 == 0) or (gcd(k1,k2) != 1) or (gcd(k2,h) != 1)
		iter += 1
	if not_found:
		return [ 0, 0, 0, 0, 0, 0, 0, 0]
	sol = k1+h
	return [ T, p1, q1, p2, q2, k1, k2, h]

def recover_ph1(mN1, mN2, T, B):
	k1pk2 = 0
	while True:
		for k1t in xsrange(0, k1pk2+2):
			k2t = k1pk2 - k1t
			g = gcd(k1t*T+mN1, k2t*T+mN2)
			if g > B:
				return [k1t, k2t, g]
		k1pk2 += 1

def recover_ph2(N1, N2, hkk1, gm, T):
	list = []
	for h in divisors(hkk1):
		k1 = hkk1 / h
		if h>1 and k1>1 and gcd(h,k1) == 1:
			q1s = Mod(gm,T)/Mod(h,T)
			q2s = q1s * h *h
			list.append([h,k1,lift(q1s),lift(q2s),gm])
			# print("(%d,%d) q1s=0x%x q2s=0x%x" % (h,k1,lift(q1s),lift(q2s)))
			q1s = Mod(T-gm,T)/Mod(h,T)
			q2s = q1s * h * h
			list.append([h,k1,lift(q1s),lift(q2s),T-gm])
			# print("(%d,%d) q1s=0x%x q2s=0x%x" % (h,k1,lift(q1s),lift(q2s)))
	# print("Found %d candidates for (h,k1)" % len(list))
	return list

def try_factorization(N,T,pi,nu,mp,mq):
	p = pi*T+mp
	q = nu*T+mq
	if N == p*q:
		return [p,q]
	return [0,0]

def get_factors(N, T, mp, mq, lb, ub):
	a = mq
	b = mp
	d = (N-mp*mq)/T
	# print("a=mq=0x%x b=mp=0x%x d=%d" % (a,b,d))
	TT = 2*T
	for C in xsrange(lb, ub+1):
		Delta = (b-a-C*T)^2-4*T*(d-b*C)
		if Delta > 0 and Delta.is_square():
			sqrtDelta = sqrt(Delta)
			n = C * T + a - b - sqrtDelta
			x = n / TT
			if x.is_integral():
				[p, q] = try_factorization(N,T,x,C-x,mp,mq)
				if p != 0:
					return [p, q]
			# n = C * T + a -b + sqrtDelta
			n += 2 * sqrtDelta
			x = n / TT
			if x.is_integral():
				[p, q] = try_factorization(N,T,x,C-x,mp,mq)
				if p != 0:
					return [p, q]
	return [0, 0]

def recover_ph3(N1, N2, T, kk2, gm, list):
	ub1 = ceil(N1/(T*T))
	lb1 = floor(sqrt(2*(floor(N1/(T*T))-1)))
	ub2 = ceil(N2/(T*T))
	lb2 = floor(sqrt(2*(floor(N2/(T*T))-1)))
	# print("High coeff. search space: [%d-%d] [%d-%d]" % (lb1,ub1,lb2,ub2))
	for x in list:
		[ h, k1, mq1, mq2, gm ] = x
		# print("Verifying candidate (%d,%d) mq1=0x%x mq2=0x%x" % (h, k1, mq1, mq2))
		[p1c, q1c] = get_factors(N1, T, (h*k1*mq2)%T, mq1, lb1, ub1)
		[p2c, q2c] = get_factors(N2, T, (kk2*mq1)%T, mq2, lb2, ub2)
		if p1c != 0 and p2c != 0:
			return [p1c, q1c, p2c, q2c]
	print("FAILED :-(")
	return [0,0,0,0]

def recover(N1, N2, T, B):
	mN1 = N1 % T
	mN2 = N2 % T
	[k1t, k2t, gm2] = recover_ph1(mN1, mN2, T, B);
	# print("k1t=%d k2t=%d\ngm2=%d" % (k1t, k2t, gm2))
	gm2inv = lift(1/Mod(gm2, T))
	# kk2=(N2%T+k2t*T)/gm2
	kk2 = (N2 * gm2inv) % T
	# hkk1=(N1%T+k1t*T)/gm2
	hkk1 = (N1 * gm2inv) % T
	gm = lift(sqrt(Mod(gm2, T)))
	# print("gm2inv=0x%x\nhkk1=%d kk2=%d\ngm=0x%x\nT-gm=0x%x" % (gm2inv, hkk1, kk2, gm, T-gm))
	list = recover_ph2(N1, N2, hkk1, gm, T)
	return recover_ph3(N1, N2, T, kk2, gm, list)

