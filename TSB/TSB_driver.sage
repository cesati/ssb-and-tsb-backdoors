# Copyright (C) 2021 Marco Cesati

from sage.doctest.util import Timer
import csv
load("TSB.sage")

#[alpha, K, c] = [  64, 100,  3]  # 128-bit moduli
#[alpha, K, c] = [ 512, 100,  3]  # 1024-bit moduli
#[alpha, K, c] = [1024,  10, 10]  # 2048-bit moduli
#[alpha, K, c] = [2048,  10, 10]  # 4096-bit modulu

timer = Timer()


def run(alpha,K,c,i):
	timer.start()
	[T, p1, q1, p2, q2, k1, k2, h] = init(alpha, K, c)
	N1=p1*q1
	N2=p2*q2
	time_gener = timer.stop().cputime
	print("T=0x%x\np1=0x%x\nq1=0x%x\np2=0x%x\nq2=0x%x\nk1=%d k2=%d h=%d" % (T,p1,q1,p2,q2,k1,k2,h))
	print("High coefficients: pi1=%d nu1=%d pi2=%d nu2=%d" % (p1//T, q1//T, p2//T, q2//T))
	# print("p1%%T=0x%x q1%%T=0x%x p2%%T=0x%x q2%%T=0x%x" % (p1%T,q1%T,p2%T,q2%T))
	# print("N1=0x%x\nN2=0x%x" % (N1,N2))
	# d1 = (N1-(p1%T)*(q1%T))/T
	# d2 = (N2-(p2%T)*(q2%T))/T
	# print("delta1=0x%x\ndelta2=0x%x" % (d1,d2))
	# C1=((p1//T)+(q1//T))
	# C2=((p2//T)+(q2//T))
	# print("C1=%d\nC2=%d" % (C1,C2))
	# Delta1=((p1%T)-(q1%T)-C1*T)^2-4*T*(d1-(p1%T)*C1)
	# Delta2=((p2%T)-(q2%T)-C2*T)^2-4*T*(d2-(p2%T)*C2)
	# print("Delta1=0x%x [%d] 0x%x" % (Delta1, Delta1.is_square(), sqrt(Delta1)))
	# print("Delta2=0x%x [%d] 0x%x" % (Delta2, Delta2.is_square(), sqrt(Delta2)))
	print("[TIMING] Generation: %.3f seconds" % time_gener)
	B = 2^(alpha-2*c)
	timer.start()
	[cp1, cq1, cp2, cq2] = recover(N1, N2, T, B)
	time_recover = timer.stop().cputime
	if cp1*cq1 == N1 and cp2*cq2 == N2:
		print("Found factors:\n  0x%x *\n  0x%x\n  0x%x *\n  0x%x" % (cp1, cq1, cp2, cq2))
	print("[TIMING] Recover: %.3f seconds" % time_recover)
	csvwriter.writerow([alpha,c,i,K,k1,k2,h,time_gener,time_recover])
	csvfile.flush()

rep = 20
alpha=512
#csvfile = open('tsb_timing.csv','wb') # older SageMath
csvfile = open('tsb_timing_512_XXX.csv','w',newline='')
K=XXX
csvwriter = csv.writer(csvfile, delimiter=',')

c=7
for x in xsrange(rep):
	run(alpha,K,c,x)

csvfile.close()

